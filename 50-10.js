const gBASE_URL = "https://62442d9d3da3ac772b0c50eb.mockapi.io/api/v1/";
var vAPI_URL = gBASE_URL + "/contacts";

var gListUser = [];

const gLIST_COL = ['id', 'name', 'email', 'createDate', 'message', 'action'];
const gID_COL = 0;
const gNAME_COL = 1;
const gEMAIL_COL = 2;
const gCREATE_DATE_COL = 3;
const gMESSAGE_COL = 4;
const gACTION_COL = 5;

var gSTT = 1;


$(document).ready(function () {
    onPageLoading();

    var vTable = $("#contact-table").DataTable({
        columns: [
            { data: gLIST_COL[gID_COL] },
            { data: gLIST_COL[gNAME_COL] },
            { data: gLIST_COL[gEMAIL_COL] },
            { data: gLIST_COL[gCREATE_DATE_COL] },
            { data: gLIST_COL[gMESSAGE_COL] },
            { data: gLIST_COL[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `
                <i class="fa-solid fa-pen-to-square text-success edit-btn" data-toggle="tooltip" data-placement="top" title="Edit" style="cursor:pointer" ></i>
                <i class="fa-solid fa-trash text-danger delete-btn" data-toggle="tooltip" data-placement="top" title="Delete" style="cursor:pointer"></i>`
            },
            {
                targets: gID_COL,
                render: () => {
                    return gSTT++;
                }
            }
        ]
    })
    loadDataIntoTable(gListUser);
})

function onPageLoading() {
    setAllUser();
    $("#add-contact-btn").click(function () {
        addContact();
    })
    $("#contact-table").on("click", ".edit-btn",function(){
        editContact(this);
    })
    $("#contact-table").on("click", ".delete-btn",function(){
        deleteContact(this);
    })

}

function setAllUser() {
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            gListUser = res;
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function loadDataIntoTable(paramList) {
    var vTable = $("#contact-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramList);
    vTable.draw();
}

function addContact() {
    $("#add-modal").modal('show');
    var vGetDate = new Date();
    var vYear = vGetDate.getFullYear();
    let vMonth = vGetDate.getMonth() + 1;
    let vDay = vGetDate.getDate();

    $("#confirm-to-create-contact-btn").click(function () {
        var vNewContact = {
            name: "",
            email: "",
            createDate: `${vDay}/${vMonth}/${vYear}`,
            message: "",
        }
        thuThapDuLieu(vNewContact);
        var vDuLieuHopLe = validate(vNewContact);
        if (vDuLieuHopLe) {
            callApiAdd(vNewContact)
        }
    })
}

function thuThapDuLieu(paramContact) {
    paramContact.name = $("#name-add-input").val();
    paramContact.email = $("#email-add-input").val();
    paramContact.message = $("#message-add-input").val();
    $("#create-date-add-inp").val(paramContact.createDate);

    console.log(paramContact);
}

// hàm kiểm tra email đúng định dạng ko?
function validateEmail(paramEmail) {
    var vValidRegex =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

function validate(paramContact) {
    if (paramContact.name == "") {
        alert("Please refill the name");
        return false;
    }
    if (paramContact.email == "") {
        alert("Please refill the email");
        return false;
    }
    if (!validateEmail(paramContact.email)) {
        alert("Invalid email");
        return false;
    }
    if (paramContact.message == "") {
        alert("Please refill the message");
        return false;
    }
    return true;
}

function callApiAdd(paramNewContact){
    $.ajax({
        url:vAPI_URL,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(paramNewContact),
        success: function(res){
            alert(`Successfully added`);
            $("#add-modal").modal('hide');
            clearModalAdd();
            setAllUser();
            loadDataIntoTable(gListUser);
        },
        error: function(error){
            console.log(error);
        }
    })
}

function clearModalAdd(){
    $("#name-add-input").val("");
    $("#email-add-input").val("");
    $("#create-date-add-inp").val("");
    $("#message-add-input").val("");
}

function editContact(paramElement){
    $("#edit-modal").modal("show");
    var vContact = getContactByElement(paramElement);
    var vUpdateContact = {
        id: vContact.id,
        name: vContact.name,
        email: vContact.email,
        createDate: vContact.createDate,
        message: vContact.message,
    };
    showDuLieuModal(vUpdateContact);
    $("#confirm-to-edit-contact-btn").click(function(){
        readEditData(vUpdateContact);
        var vDuLieuHopLe = validate(vUpdateContact);
        if(vDuLieuHopLe){
            callApiEdit(vUpdateContact);
        }
    })
}

function getContactByElement(paramElement){
    var vTable = $("#contact-table").DataTable();
    var vRow = $(paramElement).parents("tr");
    var vData = vTable.rows(vRow).data();
    console.log(vData[0]);
    return vData[0];
}

function showDuLieuModal(paramContactUpdated){
    $("#id-edit-input").val(paramContactUpdated.id);
    $("#name-edit-input").val(paramContactUpdated.name);
    $("#email-edit-input").val(paramContactUpdated.email);
    $("#create-date-edit-inp").val(paramContactUpdated.createDate);
    $("#message-edit-input").val(paramContactUpdated.message);
}

function readEditData(paramUpdate){
    paramUpdate.id = $("#id-edit-input").val();
    paramUpdate.name = $("#name-edit-input").val();
    paramUpdate.email = $("#email-edit-input").val();
    paramUpdate.createDate = $("#create-date-edit-inp").val();
    paramUpdate.message = $("#message-edit-input").val();
    console.log(paramUpdate);
}

function callApiEdit(paramContactUpdate){
    $.ajax({
        url: vAPI_URL + "/" + paramContactUpdate.id,
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(paramContactUpdate),
        success: function(res){
            alert('Successfully Updated');
            $("#edit-modal").modal("hide");
            clearModalEdit();
            setAllUser();
            loadDataIntoTable(gListUser);
        },
        error: function(error){
            console.log(error);
        }
    })
}

function clearModalEdit(){
    $("#id-edit-input").val("");
    $("name-edit-input").val("");
    $("#email-edit-input").val("");
    $("#create-date-edit-inp").val("");
    $("#message-edit-input").val("");
}

function deleteContact(paramElement){
    var vContact = getContactByElement(paramElement);
    $("#delete-modal").modal("show");
    $("#confirm-to-delete-contact-btn").click(function(){
        callApiDelete(vContact);
    })
}

function callApiDelete(paramContact){
    $.ajax({
        url: vAPI_URL + "/" + paramContact.id,
        type: "DELETE",
        success: function(res){
            alert("Successfully Deleted");
            $("#delete-modal").modal("hide");
            setAllUser();
            loadDataIntoTable(gListUser);
        },
        error: function(error){
            console.log(error);
        }
    })
}
